import { Box, Center, RadioGroup, Radio, Stack, Text } from "@chakra-ui/react";
import { useState } from "react";

export default function Quiz({
  question = "What is the capital of France?",
  choices = [
    { value: "newyork", label: "New York" },
    { value: "london", label: "London" },
    { value: "paris", label: "Paris" },
    { value: "singapore", label: "Singapore" },
  ],
  correctAnswer = "paris",
  correctNote = <>You know your capitals!</>,
  wrongNote = (
    <>
      Try checking out a <a href="http://map.google.com/">map</a>.
    </>
  ),
}) {
  const [value, setValue] = useState("");

  function find(note: String): JSX.Element {
    const selectedChoice = choices.filter(function (choice) {
      return choice.value === value;
    })[0];

    if (note === "correctNote") {
      if ("correctNote" in selectedChoice) {
        return <>{selectedChoice["correctNote"]}</>;
      }
      return <>{correctNote}</>;
    } else if (note === "wrongNote") {
      if ("wrongNote" in selectedChoice) {
        return <>{selectedChoice["wrongNote"]}</>;
      }
      return <>{wrongNote}</>;
    }
    return <></>;
  }

  return (
    <Center mx="auto">
      <Box border="1px" borderRadius="md" borderColor="theme.light" p="2">
        <Box bg="theme.green" color="theme.dark" p="1">
          <Text fontSize="xs">{question}</Text>
        </Box>
        <Box bg="theme.light" color="theme.dark" p="2">
          <RadioGroup onChange={setValue} value={value}>
            <Stack>
              {choices.map((choice) => {
                return (
                  <Radio key={choice.value} value={choice.value}>
                    {choice.label}
                  </Radio>
                );
              })}
            </Stack>
          </RadioGroup>
        </Box>
        {value.length > 0 &&
          (correctAnswer == value ? (
            <Box bg="theme.dark" color="theme.green" p="1">
              <Text fontSize="xs">Correct! {find("correctNote")}</Text>
            </Box>
          ) : (
            <Box bg="theme.dark" color="theme.red" p="1">
              <Text fontSize="xs">Incorrect. {find("wrongNote")}</Text>
            </Box>
          ))}
      </Box>
    </Center>
  );
}
