import { PPBLContext } from "@/src/context/PPBLContext";
import { METADATA_KEY_QUERY } from "@/src/data/queries/metadataQueries";
import { TREASURY_UTXO_QUERY } from "@/src/data/queries/treasuryQueries";
import { faucetRegistryMetadata } from "@/src/types/faucet";
import { useQuery, gql, useLazyQuery } from "@apollo/client";
import {
  Box,
  Center,
  Divider,
  Flex,
  Grid,
  Heading,
  Spacer,
  Spinner,
  Stack,
  Text,
  useDisclosure,
  useToast,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Button,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionIcon,
  AccordionPanel,
} from "@chakra-ui/react";
import {
  Data,
  resolvePaymentKeyHash,
  Transaction,
  UTxO,
  Asset,
  resolveScriptRef,
  resolveFingerprint,
  PlutusScript,
  Action,
} from "@meshsdk/core";
import { useAddress, useWallet } from "@meshsdk/react";
import { useContext, useEffect, useState } from "react";
import faucetProject from "@/src/cardano/faucet-mini-project/faucets-plutus.json";
import { hexToString, stringToHex } from "@/src/utils";

interface FaucetContractList {
  [key: string]: string;
}

const faucetContractList: FaucetContractList = faucetProject.faucets;

type Props = {
  faucetMetadata: faucetRegistryMetadata;
};

const REF_UTXO_VALUE_QUERY = gql`
  query GetReferenceUTxOValue($txHash: Hash32Hex!, $index: Int!) {
    utxos(where: { _and: [{ transaction: { hash: { _eq: $txHash } } }, { index: { _eq: $index } }] }) {
      address
      value
    }
  }
`;

const FAUCET_UTXO_QUERY = gql`
query GetFaucetUTxOs($contractAddress: String!, $assetID: Hex!) {
  utxos(
    where: {
      _and: [
        { address: { _eq: $contractAddress } },
        { tokens: { asset: { assetId: { _eq: $assetID } } } }
      ]
    }
  ) {
    txHash
    index
    tokens {
      quantity
    }
    datum {
      bytes
      value
    }
  }
}
`;

function addressMetadataFix(address: any) {
  if (Array.isArray(address)) {
    address = address[0] + address[1]
    console.log(address)
  }
  return address
}

function tokenNameMetadataFix(policyId: string, name: string) {
  if (resolveFingerprint(policyId, name) == resolveFingerprint(policyId, hexToString(name))) {
    name = hexToString(name)// + " (hex)"
  }
  return name
}

const FaucetDynamicInstance: React.FC<Props> = ({ faucetMetadata }) => {
  // ---------------------------------------------------------------------------------------
  // Depends on metadata registry + git merge to project
  //
  // Submit a merge request adding a line to faucets-plutus.json
  // ---------------------------------------------------------------------------------------
  const contractAddress = addressMetadataFix(faucetMetadata.contractAddress);

  const faucetPolicyId = faucetMetadata.policyId;
  const faucetTokenName = tokenNameMetadataFix(faucetMetadata.policyId, faucetMetadata.tokenName);
  const faucetAsset = faucetPolicyId + stringToHex(faucetTokenName);
  const withdrawalAmount = faucetMetadata.withdrawalAmount;
  const refUTxOHash = faucetMetadata.refUTxOHash;
  const refUTxOIx = faucetMetadata.refUTxOIx;
  const aboutToken = faucetMetadata.aboutToken;


  const outgoingDatum: Data = {
    alternative: 0,
    fields: [withdrawalAmount, faucetTokenName],
  };

  const _faucetPlutusScript: PlutusScript = {
    version: "V2",
    code: faucetContractList[contractAddress],
  };

  console.log(contractAddress)
  console.log(_faucetPlutusScript)

  // ---------------------------------------------------------------------------------------
  // Template:
  // ---------------------------------------------------------------------------------------

  const { connected, wallet } = useWallet();
  const address = useAddress();
  const [connectedPkh, setConnectedPkh] = useState<string | undefined>(undefined);
  const [redeemer, setRedeemer] = useState<Partial<Action> | undefined>(undefined);

  const ppblContext = useContext(PPBLContext);

  // Faucet Hooks:
  const [faucetBalance, setFaucetBalance] = useState<string | undefined>(undefined);
  const [newFaucetBalance, setNewFaucetBalance] = useState<string | undefined>(undefined);
  const [faucetAssetAtFaucetContract, setFaucetAssetAtFaucetContract] = useState<Asset[] | undefined>(undefined);
  const [faucetAssetToFaucetContract, setFaucetAssetToFaucetContract] = useState<Asset[] | undefined>(undefined);
  const [faucetAssetToBrowserWallet, setFaucetAssetToBrowserWallet] = useState<Asset[] | undefined>(undefined);
  const [numFaucetUTxOs, setNumFaucetUTxOs] = useState(0);
  const [inputFaucetUTxO, setInputFaucetUTxO] = useState<UTxO | undefined>(undefined);
  const [outputFaucetUTxO, setOutputFaucetUTxO] = useState<Partial<UTxO> | undefined>(undefined);
  const [referenceUTxO, setReferenceUTxO] = useState<UTxO | undefined>(undefined);

  const [successTxHash, setSuccessTxHash] = useState<string | undefined>(undefined);

  // For Chakra Modal:
  const { isOpen: isConfirmationOpen, onOpen: onConfirmationOpen, onClose: onConfirmationClose } = useDisclosure();
  const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure();
  const toast = useToast();

  const {
    data: faucetQueryData,
    loading: faucetQueryLoading,
    error: faucetQueryError,
  } = useQuery(FAUCET_UTXO_QUERY, {
    variables: {
      contractAddress: contractAddress,
      assetID: faucetAsset,
    },
  });

  const {
    data: refUTxOQueryData,
    loading: refUTxOQueryLoading,
    error: refUTxOQueryError,
  } = useQuery(REF_UTXO_VALUE_QUERY, {
    variables: {
      txHash: faucetMetadata.refUTxOHash,
      index: faucetMetadata.refUTxOIx,
    },
  });

  useEffect(() => {
    if (address) {
      setConnectedPkh(resolvePaymentKeyHash(address));
    }
  }, [address]);

  useEffect(() => {
    if (connectedPkh && ppblContext) {
      const _redeemer: Partial<Action> = {
        data: {
          alternative: 0,
          fields: [connectedPkh, "222" + ppblContext.connectedContribToken],
        },
      };
      setRedeemer(_redeemer);
    }
  }, [connectedPkh, ppblContext]);

  useEffect(() => {
    if (faucetQueryData) {
      setNumFaucetUTxOs(faucetQueryData.utxos?.length);
    }

    if (faucetQueryData && faucetQueryData.utxos && faucetQueryData.utxos.length > 0 && faucetQueryData.utxos[0].tokens[0]) {
      const _tokenBalance = faucetQueryData.utxos[0].tokens[0].quantity;
      const _newTokenBalance = parseInt(_tokenBalance) - withdrawalAmount;
      setFaucetBalance(_tokenBalance);
      setNewFaucetBalance(_newTokenBalance.toString());
    }
  }, [faucetQueryData]);

  useEffect(() => {
    if (refUTxOQueryData && refUTxOQueryData.utxos.length == 1 && _faucetPlutusScript) {
      const _referenceUTxO: UTxO = {
        input: {
          outputIndex: refUTxOIx,
          txHash: refUTxOHash,
        },
        // query for refUTxO address and lovelace amount
        output: {
          address: refUTxOQueryData.utxos[0].address,
          amount: [{ unit: "lovelace", quantity: refUTxOQueryData.utxos[0].value }],
          scriptRef: _faucetPlutusScript.code,
        },
      };
      setReferenceUTxO(_referenceUTxO);
    }
  }, [refUTxOQueryData]);

  useEffect(() => {
    if (faucetBalance && newFaucetBalance) {
      const _faucetAssetToBrowserWallet: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: withdrawalAmount.toString(),
        },
      ];
      const _faucetAssetAtFaucetContract: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: faucetBalance,
        },
      ];
      const _faucetAssetToFaucetContract: Asset[] = [
        { unit: "lovelace", quantity: "2000000" },
        {
          unit: faucetAsset,
          quantity: newFaucetBalance,
        },
      ];

      setFaucetAssetToBrowserWallet(_faucetAssetToBrowserWallet);
      setFaucetAssetAtFaucetContract(_faucetAssetAtFaucetContract);
      setFaucetAssetToFaucetContract(_faucetAssetToFaucetContract);
    }
  }, [faucetBalance, newFaucetBalance]);

  useEffect(() => {
    if (
      faucetAssetAtFaucetContract &&
      faucetAssetToFaucetContract &&
      faucetQueryData.utxos &&
      faucetQueryData.utxos.length > 0 &&
      faucetQueryData.utxos[0].datum
    ) {
      const _inputFaucetUTxO: UTxO = {
        input: {
          txHash: faucetQueryData.utxos[0].txHash,
          outputIndex: faucetQueryData.utxos[0].index,
        },
        output: {
          address: contractAddress,
          amount: faucetAssetAtFaucetContract,
          plutusData: faucetQueryData.utxos[0].datum.bytes,
        },
      };
      const _outputFaucetUTxO: Partial<UTxO> = {
        output: {
          address: contractAddress,
          amount: faucetAssetToFaucetContract,
        },
      };

      setInputFaucetUTxO(_inputFaucetUTxO);
      setOutputFaucetUTxO(_outputFaucetUTxO);
    }
  }, [faucetAssetAtFaucetContract, faucetAssetToFaucetContract]);

  // ---------------------------------------------------------------------------------------
  if (faucetQueryLoading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (faucetQueryError) {
    console.error(faucetQueryError);
    return (
      <Center flexDirection="column">
        <Heading size="lg">Error loading data...</Heading>;<pre>{JSON.stringify(faucetQueryError, null, 2)}</pre>
      </Center>
    );
  }

  const handleFaucetTx = async () => {
    try {
      console.log("click!");
      if (address && faucetAssetToBrowserWallet && inputFaucetUTxO && outputFaucetUTxO) {
        const tx = new Transaction({ initiator: wallet })
          .redeemValue({
            value: inputFaucetUTxO,
            script: referenceUTxO,
            datum: inputFaucetUTxO,
            redeemer: redeemer,
          })
          .sendValue(
            {
              address: contractAddress,
              datum: {
                value: outgoingDatum,
                inline: true,
              },
            },
            outputFaucetUTxO
          )
          .sendAssets(address, faucetAssetToBrowserWallet)
          .sendAssets(address, [
            { unit: "lovelace", quantity: "2000000" },
            { unit: ppblContext.connectedContribTokenUnit, quantity: "1" },
          ]);

        console.log("Your Tx: ", tx);
        const unsignedTx = await tx.build();
        const signedTx = await wallet.signTx(unsignedTx, true);
        const txHash = await wallet.submitTx(signedTx);
        console.log(txHash);
        onConfirmationClose();
        onSuccessOpen();
        setSuccessTxHash(txHash);
      }
    } catch (error: any) {
      if (error.info) {
        alert(error.info);
        console.log(error.info);
        console.log(error);
      } else {
        console.log(error);
      }
    }
  };

  if (!faucetContractList[contractAddress])
    return (
      <Box p="4" my="3" border="1px" borderColor="theme.light" borderRadius="lg" bg="purple.900" color="blue.100">
        <Text>Contract Address</Text>
        <Text>{contractAddress}</Text>
        <Text>does not exist in faucets-plutus.json.</Text>
        <Text>Please submit a merge request adding a Plutus script to faucets-plutus.json.</Text>
      </Box>
    );

  return (
    <>
      <Box p="4" my="3" borderLeft="2px" borderColor="theme.light" bgGradient="linear(to-r, purple.900, purple.500)" color="green.100">
        <Flex direction={{ base: "column", lg: "row" }}>
          <Heading>{faucetTokenName}</Heading>
          <Spacer />
          <Box>
            <Text>Faucet UTxOs: {numFaucetUTxOs}</Text>
            <Text>Faucet Balance: {faucetBalance}</Text>
          </Box>
        </Flex>
        <Button mb="3" onClick={onConfirmationOpen} size="md" bg="theme.green" color="theme.dark">
          Withdraw {withdrawalAmount} {faucetTokenName} tokens
        </Button>
        <Text mb="3">{faucetTokenName} PolicyId: {faucetPolicyId}</Text>
        <Accordion allowMultiple>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Redeemer</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(redeemer, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Faucet UTxO</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <Text>Input</Text>
              <pre>{JSON.stringify(inputFaucetUTxO, null, 2)}</pre>
              <Text>Output</Text>
              <pre>{JSON.stringify(outputFaucetUTxO, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Reference UTxO</Heading>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(referenceUTxO, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Datum</Heading>
              <AccordionIcon />
            </AccordionButton>

            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(outgoingDatum, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Heading size="sm">Faucet Metadata</Heading>
              <AccordionIcon />
            </AccordionButton>

            <AccordionPanel pb={4}>
              <pre>{JSON.stringify(faucetMetadata, null, 2)}</pre>
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Box>
      <Modal blockScrollOnMount={false} isOpen={isConfirmationOpen} onClose={onConfirmationClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirmation Faucet Withdrawal</ModalHeader>
          <ModalBody></ModalBody>

          <ModalFooter>
            <Spacer />
            <Button bg="white" color="gray.700" onClick={handleFaucetTx}>
              Confirm
            </Button>
            <Button bg="white" color="gray.700" onClick={onConfirmationClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal blockScrollOnMount={false} isOpen={isSuccessOpen} onClose={onSuccessClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Success!</ModalHeader>
          <ModalBody>
            <Text py="2">Transaction ID: {successTxHash}</Text>
            <Text py="2">It may take a few minutes for this tx to show up on a blockchain explorer.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="white" color="gray.700" onClick={onSuccessClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default FaucetDynamicInstance;
