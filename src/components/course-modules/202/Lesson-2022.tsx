import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module202.json";
import Docs2022 from "@/src/components/course-modules/202/Docs2022.mdx";

export default function Lesson2022() {
  const slug = "2022";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={202} sltId="202.2" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2022 />
    </LessonLayout>
  );
}
