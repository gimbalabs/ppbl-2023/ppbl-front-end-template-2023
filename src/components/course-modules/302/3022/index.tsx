import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";
import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";

import module from "../module.json";
import Docs from "./Docs.mdx";

export default function Lesson3022() {
    const slug = "3022";
    const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

    return (
        <LessonLayout moduleNumber={302} sltId="302.2" slug={slug}>
            <LessonIntroAndVideo lessonData={lessonDetails} />
            <Docs />
        </LessonLayout>
    );
}
