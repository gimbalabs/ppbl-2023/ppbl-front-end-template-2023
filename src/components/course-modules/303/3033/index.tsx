import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";
import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";

import module from "../module.json";
import Docs from "./Docs.mdx";

export default function Lesson3033() {
    const slug = "3033";
    const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

    return (
        <LessonLayout moduleNumber={303} sltId="303.3" slug={slug}>
            <LessonIntroAndVideo lessonData={lessonDetails} />
            <Docs />
        </LessonLayout>
    );
}
