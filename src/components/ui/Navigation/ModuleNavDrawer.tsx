import {
  Box,
  Center,
  Text,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Link as CLink,
  useDisclosure,
} from "@chakra-ui/react";
import NarrowModuleList from "@/src/components/lms/Course/NarrowModuleList";
import { MdLibraryBooks } from "react-icons/md";

export default function ModuleNavDrawer() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Box position="fixed" top="125px" right="0">
        <Center
          fontWeight="light"
          fontSize="xl"
          px={{ base: "10px" }}
          py="2"
          bg="theme.green"
          color="theme.dark"
          as="button"
          onClick={onOpen}
          _hover={{ bg: "theme.lightGray", color: "theme.green" }}
          borderLeftRadius="lg"
          flexDirection="row"
        >
          <MdLibraryBooks />
          <Text pl="3" fontSize="md">
            Course Modules
          </Text>
        </Center>
      </Box>
      <Drawer isOpen={isOpen} placement="right" onClose={onClose} size="md">
        <DrawerOverlay />
        <DrawerContent bg="theme.lightGray" color="theme.light">
          <DrawerCloseButton />
          <DrawerHeader>PPBL 2023 Module List</DrawerHeader>
          <DrawerBody>
            <NarrowModuleList />
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
}
