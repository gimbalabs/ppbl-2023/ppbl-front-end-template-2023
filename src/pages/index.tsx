import Head from "next/head";
import GetHelp from "@/src/components/lms/Course/GetHelp";
import Hero from "@/src/components/ui/Text/Hero";
import {
  Box,
  Center,
  Divider,
  Text,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Link as CLink,
  useDisclosure,
  DrawerFooter,
  Button,
  Flex,
} from "@chakra-ui/react";
import LanguageList from "@/src/components/lms/Course/LanguageList";
import { useRef } from "react";
import NarrowModuleList from "@/src/components/lms/Course/NarrowModuleList";
import { BsBook } from "react-icons/bs";
import { MdLibraryBooks } from "react-icons/md";

export default function Home() {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {/* <Box position="fixed" top="125px" right="0">
        <Center
          fontWeight="light"
          fontSize="xl"
          px={{ base: "10px", md: "70px" }}
          py="2"
          bg="theme.green"
          color="theme.dark"
          as="button"
          onClick={onOpen}
          _hover={{ bg: "theme.lightGray", color: "theme.green" }}
          borderLeftRadius="lg"
          flexDirection="row"
        >
          <MdLibraryBooks />
          <Text pl="3" fontSize="md">
            Jump to a Module
          </Text>
        </Center>
      </Box> */}
      <Hero />
      <LanguageList />
      <Divider w="70%" mx="auto" pb="10" />
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <GetHelp />
      </Box>
      <Divider w="70%" mx="auto" />
      {/* <Drawer isOpen={isOpen} placement="right" onClose={onClose} size="md">
        <DrawerOverlay />
        <DrawerContent bg="theme.lightGray" color="theme.light">
          <DrawerCloseButton />
          <DrawerHeader>PPBL 2023 Module List</DrawerHeader>

          <DrawerBody>
            <NarrowModuleList />
          </DrawerBody>
        </DrawerContent>
      </Drawer> */}
    </>
  );
}
