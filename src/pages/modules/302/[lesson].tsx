import Lesson3021 from "@/src/components/course-modules/302/3021";
import Lesson3022 from "@/src/components/course-modules/302/3022";
import Lesson3023 from "@/src/components/course-modules/302/3023";
import Assignment302Page from "@/src/components/course-modules/302/assignment302";
import SLTs302 from "@/src/components/course-modules/302/slts";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import slt from "@/src/data/slts-english.json";

const Module302Lessons = () => {
    const moduleSelected = slt.modules.find((m) => m.number === 302);

    const status = null;

    const lessons = [
        { key: "slts", component: <SLTs302 /> },
        { key: "3021", component: <Lesson3021 /> },
        { key: "3022", component: <Lesson3022 /> },
        { key: "3023", component: <Lesson3023 /> },
        { key: "assignment302", component: <Assignment302Page /> },
    ];

    return (
        <ModuleLessons
            items={moduleSelected?.lessons ?? []}
            modulePath="/modules/302"
            selected={0}
            lessons={lessons}
            status={status}
        />
    );
};

export default Module302Lessons;
