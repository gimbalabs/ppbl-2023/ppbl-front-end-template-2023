import { Heading, Box, Link as CLink, Text, Divider, List, ListItem, OrderedList } from "@chakra-ui/react";
import Head from "next/head";

export default function GovernancePage() {
  return (
    <>
      <Head>
        <title>Gimbalabs PBL Governance</title>
        <meta name="description" content="About Plutus Project-Based Learning" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={{ base: "95%", md: "85%", lg: "75%" }} mx="auto">
        <Heading py="10" size="2xl">
          Gimbalabs PBL Governance Sessions
        </Heading>
        <Text fontSize="xl" py="3">
          In January + February 2024, Plutus PBL Live Coding will be replaced by Gimbalabs PBL Governance Sessions.
        </Text>
        <Text fontSize="xl" py="3">
          PBL Governance Sessions will meet on Wednesdays and Thursdays from 1430 to 1600 UTC, just like Live Coding.
        </Text>
        <Heading py="5" mt="5" borderTop="1px">
          Guiding Questions:
        </Heading>
        <OrderedList fontSize="xl">
          <ListItem py="2" pl="3" ml="3">
            What&apos;s next for Plutus PBL?
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            What&apos;s next for Gimbalabs?
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            Are there other &quot;Project-Based Learning&quot; courses we should prioritize building?
          </ListItem>
        </OrderedList>
        <Heading py="5" mt="5" borderTop="1px">
          Who Should Participate?
        </Heading>
        <Text fontSize="xl" py="3">
          Everyone is welcome. In particular, you should join if:
        </Text>
        <OrderedList fontSize="xl">
          <ListItem py="2" pl="3" ml="3">
            You participated in Plutus PBL 2023 and want to contribute at Gimbalabs or get involved in some of our
            ongoing projects.
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            You want to help decide on the priorities for Gimbalabs in 2024.
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            You want to collaborate in practicing <CLink href="https://sociocracy30.org">Sociocracy 3.0</CLink>.
          </ListItem>
        </OrderedList>
        <Heading py="5" mt="5" borderTop="1px">
          Registration:
        </Heading>
        <Text fontSize="xl" py="3">
          Meetings will take place on Zoom. To register,{" "}
          <CLink href="https://us06web.zoom.us/meeting/register/tZErceCqpzMtG9XldfuPnBQEus5MBivl9OZe">click here</CLink>
          .
        </Text>
        <Text fontSize="xl" py="3">
          Meetings will take place at the same Zoom link as Live Coding. If you subscribe to the{" "}
          <CLink href="https://gimbalabs.com/calendar">Gimbalabs Calendar</CLink>, you don&apos;t have to change
          anything.
        </Text>
        <Heading py="5" mt="5" borderTop="1px">
          Upcoming Sessions:
        </Heading>

        <Text fontSize="xl" py="3">
          #1 - Driver Mapping Workshop (Part 1): Wednesday, 10 January, 2024 at 1430 UTC.
        </Text>
        <Text fontSize="xl" py="3">
          #2 - Driver Mapping Workshop (Part 2): Thursday, 11 January, 2024 at 1430 UTC.
        </Text>
        {/* tell ODIN Discord! */}
        <Text fontSize="xl" py="10">
          <CLink href="'https://discord.gg/Va7DXqSSn8">Join Gimbalabs on Discord</CLink> for announcements
        </Text>
      </Box>
    </>
  );
}

// https://us06web.zoom.us/meeting/register/tZErceCqpzMtG9XldfuPnBQEus5MBivl9OZe
