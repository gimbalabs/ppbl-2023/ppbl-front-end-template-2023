import { Heading, Box, Text, Table, TableContainer, Thead, Th, Tbody, Tr, Td, Link as CLink } from "@chakra-ui/react";
import Head from "next/head";

import events from "@/src/data/live-coding/events-english.json";
import Link from "next/link";

export default function LiveCoding() {
  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="Gimbalabs Live Coding Sessions" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={["95%"]} mx="auto">
        <Heading py="10" size="2xl">
          Plutus PBL Live Coding
        </Heading>

        <Text fontSize="xl" w="80%" my="3">
          Updated 2024-01-01
        </Text>
        <Text fontSize="xl" w="80%" my="3">
          Happy New Year!
        </Text>
        <Text fontSize="xl" w="80%" my="3">
         Plutus PBL Live Coding meets on Wednesdays and Thursdays at 1430 UTC. You can see an archive of past sessions below. In January and February 2024, we will focus on contribution and course governance.
        </Text>
        <Text fontSize="xl" w="80%" my="3">
          
        </Text>
        
        <Heading size="xl" py="5">
          Plutus PBL 2023 Live Coding Archive
        </Heading>
        <TableContainer pb="10">
          <Table size="sm">
            <Thead>
              <Tr>
                <Th>Date</Th>
                <Th>Title</Th>
                <Th>Description</Th>
                <Th>Recording</Th>
              </Tr>
            </Thead>
            <Tbody>
              {events.events
                .filter((ev: any) => ev.complete)
                .map((event: any, i) => (
                  <Tr key={i}>
                    <Td>{event.date}</Td>
                    <Td>{event.title}</Td>
                    {event.links && event.links[0] ? (
                      <Td color="theme.yellow">
                        <Link href={event.links[0]}>{event.description}</Link>
                      </Td>
                    ) : (
                      <Td>{event.description}</Td>
                    )}
                    <Td>
                      <CLink href={event.recordingLink} target="_blank" color="theme.yellow">
                        View Meeting Recording
                      </CLink>
                    </Td>
                  </Tr>
                ))}
            </Tbody>
          </Table>
        </TableContainer>
        {/* <Divider py="5" />
        <Heading>Gimbalabs Indonesia</Heading>
        <Divider py="5" />
        <Heading>Gimbalabs Vietnam</Heading> */}
      </Box>
    </>
  );
}
