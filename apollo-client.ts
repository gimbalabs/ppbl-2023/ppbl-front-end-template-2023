import { ApolloClient, from, HttpLink, InMemoryCache } from "@apollo/client";
import { onError } from "@apollo/client/link/error";

const awaitableFetch = async (uri: RequestInfo | URL, options: RequestInit & {start?: Date} | undefined) => {
    
    if (options?.start) {
        const delay = (ms: number) => new Promise(res => setTimeout(res, ms));
        await(delay(options.start.valueOf() - Date.now()));
    }

    return fetch(uri, options)
}

const httpLink = new HttpLink({uri: "https://d.graphql-api.iohk-preprod.dandelion.link/", fetch: awaitableFetch});

const errorLink = onError(({graphQLErrors, forward, operation}) => {
    if (graphQLErrors) {
        for (let err of graphQLErrors) {
            if (err.message.startsWith("Cannot return null for non-nullable field")){
                const context = operation.getContext();
                const start = new Date(Date.now() + 250);
                const retry = (context.retry ?? 0) + 1;
                operation.setContext({...context, retry, fetchOptions: {...context.fetchOptions, start}});
                if (retry < 20) {
                    return forward(operation);
                }
            }
        }
    }
});

// To work with other networks, change the uri to any Dandelion GraphQL instance:
const client = new ApolloClient({
    link: from([errorLink, httpLink]),
    cache: new InMemoryCache(),
});

export default client;